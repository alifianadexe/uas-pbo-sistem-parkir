/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Koneksi;

import com.mysql.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class KoneksiDB {
    public static Connection Koneksi;

    public static Connection getConnection() throws SQLException {
        if (Koneksi == null) {
            new Driver();
            Koneksi = DriverManager.getConnection("jdbc:mysql://localhost:3306/parkir", "root", "");
        }
        return Koneksi;
    }

}
